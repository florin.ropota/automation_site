package com.florinauto.model;

public class PaymentInfo {

    private String holderName;
    private String cardNumber;
    private String CVV;
    private String expMonth;
    private String expYear;

    public PaymentInfo(String holderName, String cardNumber, String CVV, String expMonth, String expYear) {
        this.holderName = holderName;
        this.cardNumber = cardNumber;
        this.CVV = CVV;
        this.expMonth = expMonth;
        this.expYear = expYear;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCVV() {
        return CVV;
    }

    public void setCVV(String CVV) {
        this.CVV = CVV;
    }

    public String getExpMonth() {
        return expMonth;
    }

    public void setExpMonth(String expMonth) {
        this.expMonth = expMonth;
    }

    public String getExpYear() {
        return expYear;
    }

    public void setExpYear(String expYear) {
        this.expYear = expYear;
    }
}
