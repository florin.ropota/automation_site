package com.florinauto.model;

public class UserInfo {

   public String username;

   public String email;

   public String gender;

   public String title;

   public String password;

   public String dateOfBirth;

   public String firstName;

   public String lastName;

   public String address;

   public String country;

   public String state;

   public String city;

   public String zipCode;

   public String mobileNumber;

    public UserInfo(String username,String email,String gender,String title, String password,String dateOfBirth,String firstName,String lastName,String address,String country,String state,String city,String zipCode,String mobileNumber)
    {
        this.username=username;
        this.email=email;
        this.gender=gender;
        this.gender=title;
        this.password=password;
        this.dateOfBirth=dateOfBirth;
        this.firstName=firstName;
        this.lastName=lastName;
        this.address=address;
        this.country=country;
        this.state=state;
        this.city=city;
        this.zipCode=zipCode;
        this.mobileNumber=mobileNumber;

    }

    public UserInfo(String title,String firstName,String lastName,String address,String country,String state,String city,String zipCode,String mobileNumber)
    {
        this.title=title;
        this.firstName=firstName;
        this.lastName=lastName;
        this.address=address;
        this.country=country;
        this.state=state;
        this.city=city;
        this.zipCode=zipCode;
        this.mobileNumber=mobileNumber;

    }
}

