package com.florinauto.page_object;

import com.florinauto.model.LoginInfo;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;

public class LoginPage {

    private WebDriver driver;
    private By emailInput=By.cssSelector("[data-qa=\"login-email\"]");
    private By passwordInput=By.cssSelector("[data-qa=\"login-password\"]");
    private By LoginButton=By.cssSelector("[data-qa=\"login-button\"]");
    private By LogoutButton=By.cssSelector("[href=\"/logout\"]");

    private By noLogin=By.cssSelector("[action=\"/login\"]");

    public LoginPage(WebDriver driver) {this.driver = driver;}

    public void addLoginDetails(LoginInfo loginInfo)
    {
        driver.findElement(emailInput).sendKeys(loginInfo.getEmail());
        driver.findElement(passwordInput).sendKeys(loginInfo.getPassword());
    }

    public void clickLogin()
    {
        driver.findElement(LoginButton).click();
    }

    public String verifySuccessLogin()
    {
        return driver.findElement(LogoutButton).getText();
    }

    public void logout()
    {
        driver.findElement(LogoutButton).click();
    }
    public String verifySuccessLogout()
    {
       return driver.findElement(LoginButton).getText();
    }

    public boolean verifyWrongLogin()
    {
        return driver.findElement(noLogin).getText().contains("Your email or password is incorrect!");
    }
}
