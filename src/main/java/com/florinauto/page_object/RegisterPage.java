package com.florinauto.page_object;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Month;
import java.util.Date;

import java.util.List;

import static org.apache.commons.lang3.math.NumberUtils.toInt;

public class RegisterPage {
    private WebDriver driver;

    private By usernameInput= By.cssSelector("[placeholder=\"Name\"]");

    private By emailInput=By.cssSelector("[data-qa=\"signup-email\"]");

    private By signupButton=By.cssSelector("[data-qa=\"signup-button\"]");

    private By genderMale=By.id("id_gender1");

    private By genderFemale=By.id("id_gender2");

    private By passwordInput=By.id("password");

    private By firstNameInput=By.id("first_name");

    private By lastNameInput=By.id("last_name");

    private By addressInput=By.id("address1");

    private By stateInput=By.id("state");

    private By cityInput=By.id("city");

    private By zipcodeInput=By.id("zipcode");

    private By mobileNumberInput=By.id("mobile_number");

    private By createAccount=By.cssSelector("[data-qa=\"create-account\"]");

    private By accountCreated=By.cssSelector("[data-qa=\"account-created\"]");

    private By countryInput=By.id("country");
    private By daysInput=By.id("days");
    private By monthsInput=By.id("months");
    private By yearsInput=By.id("years");



    public RegisterPage(WebDriver driver) {
        this.driver = driver;
    }

    public void fillUsername(String username) {
        driver.findElement(usernameInput).sendKeys(username);
    }

    public void fillEmail(String email) {
        driver.findElement(emailInput).sendKeys(email);
    }

    public void clickSignupButton() {driver.findElement(signupButton).click();}

    public void selectGender(String gender)
    {
        if (gender.toLowerCase().equals("male"))
        { driver.findElement(genderMale).click();}
        else driver.findElement(genderFemale).click();
    }

    public boolean checkSignupWasSuccessful()
    {
        if (driver.getCurrentUrl().equals("https://automationexercise.com/signup")) return true;
        else return false;
    }
    public void fillPassword(String password) {
        driver.findElement(passwordInput).sendKeys(password);
    }

    public void fillBirthDate(String birthDate)  {

        String date[] = birthDate.split("-");
        System.out.println(date[2]+date[1]+date[0]);
        Select dropdownDays = new Select(driver.findElement(daysInput));
        dropdownDays.selectByValue(date[2].replaceFirst ("^0*", ""));

        Select dropdownMonths = new Select(driver.findElement(monthsInput));
        dropdownMonths.selectByValue(date[1].replaceFirst ("^0*", ""));
     //   Month month=Month.of(toInt(date[1].replaceFirst ("^0*", "")));
     //   dropdownMonths.selectByValue(month.toString().toLowerCase().replaceFirst(".",month.toString().substring(0,1).toUpperCase()));

        Select dropdownYears = new Select(driver.findElement(yearsInput));
        dropdownYears.selectByValue(date[0]);

    }
    public void fillFirstName(String firstName) {
        driver.findElement(firstNameInput).sendKeys(firstName);
    }
    public void fillLastName(String lastName) {
        driver.findElement(lastNameInput).sendKeys(lastName);
    }
    public void fillAddress(String address) {
        driver.findElement(addressInput).sendKeys(address);
    }

    public void fillCountry(String country){
        Select dropdownCountries = new Select(driver.findElement(countryInput));
        dropdownCountries.selectByValue(country);
    }

    public void fillState(String state) {
        driver.findElement(stateInput).sendKeys(state);
    }
    public void fillCity(String city) {
        driver.findElement(cityInput).sendKeys(city);
    }

    public void fillZipCode(String zipcode) {
        driver.findElement(zipcodeInput).sendKeys(zipcode);
    }

    public void fillMobileNumber(String mobileNumber) {
        driver.findElement(mobileNumberInput).sendKeys(mobileNumber);
    }

  public void clickCreateAccount()
  {
      JavascriptExecutor js = (JavascriptExecutor) driver;
     js.executeScript("window.scrollBy(0,300)", "");
      driver.findElement(createAccount).click();
  }

    public String accountCreated()
    {
        return driver.findElement(accountCreated).getText();
    }
}
