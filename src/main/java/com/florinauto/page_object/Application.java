package com.florinauto.page_object;

import org.openqa.selenium.WebDriver;

public class Application {

    public WebDriver driver;

    public Application(WebDriver driver) {
        this.driver = driver;
    }

    public RegisterPage navigateToRegisterPage() {
        driver.get("https://automationexercise.com/login");
        return new RegisterPage(driver);
    }

    public LoginPage navigateToLoginPage() {
        driver.get("https://automationexercise.com/login");
        return new LoginPage(driver);
    }
    public MainPage navigateToMainPage() {
        driver.get("https://automationexercise.com/");
        return new MainPage(driver);
    }
}
