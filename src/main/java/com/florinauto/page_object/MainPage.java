package com.florinauto.page_object;
import com.florinauto.page_object.LoginPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MainPage {
    private WebDriver driver;

    private By SignupLogin=By.cssSelector("[href=\"/login\"]");
    private By productsButton=By.cssSelector("[href=\"/products\"]");
    private By cartButton=By.cssSelector("[href=\"/view_cart\"]");
    private By deletedButton=By.cssSelector("[href=\"/delete_account\"]");

    public MainPage(WebDriver driver) {this.driver = driver;}

    public WebDriver clickLoginSignup()
    {
        driver.findElement(SignupLogin).click();
        return driver;
    }

    public WebDriver clickViewProducts()
    {
        driver.findElement(productsButton).click();
        return driver;
    }
    public WebDriver clickViewCart()
    {
        driver.findElement(cartButton).click();
        return driver;
    }

    public void deleteAccount()
    {
        driver.findElement(deletedButton).click();
    }
}
