package com.florinauto.page_object;

import com.florinauto.model.PaymentInfo;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import java.io.File;

public class PaymentPage {

    private WebDriver driver;
    private By nameInput=By.cssSelector("[name=\"name_on_card\"]");
    private By cardNumberInput=By.cssSelector("[name=\"card_number\"]");
    private By cvvInput=By.cssSelector("[name=\"cvc\"]");
    private By monthInput=By.cssSelector("[name=\"expiry_month\"]");
    private By yearInput=By.cssSelector("[name=\"expiry_year\"]");
    private By payButton=By.cssSelector("[data-qa=\"pay-button\"]");

    private By confirmMessage=By.cssSelector("[data-qa=\"order-placed\"]");
    private By downloadInvoiceButton=By.cssSelector("[href^=\"/download_invoice\"]");

    private By deleteAccountButton=By.cssSelector("[href=\"/delete_account\"]");

    public PaymentPage(WebDriver driver) {
        this.driver = driver;
    }

    public void fillPaymentDetails(PaymentInfo payInfo)
    {
        driver.findElement(nameInput).sendKeys(payInfo.getHolderName());
        driver.findElement(cardNumberInput).sendKeys(payInfo.getHolderName());
        driver.findElement(cvvInput).sendKeys(payInfo.getCVV());
        driver.findElement(monthInput).sendKeys(payInfo.getExpMonth());
        driver.findElement(yearInput).sendKeys(payInfo.getExpYear());
        driver.findElement(payButton).click();
    }

    public String orderPlaced()
    {
        return driver.findElement(confirmMessage).getText();
    }

    public boolean downloadInvoice()
    {
        driver.findElement(downloadInvoiceButton).click();
        File f = new File("c:\\Users\\florin.ropota\\Downloads\\invoice.txt");
        return f.exists();
    }
    public void deleteAccount()
    {
        driver.findElement(deleteAccountButton).click();
    }

}
