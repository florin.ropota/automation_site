package com.florinauto.page_object;

import com.florinauto.model.UserInfo;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import java.util.List;

public class CheckoutPage {
    private WebDriver driver;
    private By paymentsButton = By.cssSelector("[href=\"/payment\"]");

    private By deliveryAddress = By.id("address_delivery");

    public CheckoutPage(WebDriver driver) {
        this.driver = driver;
    }

    public UserInfo deliveryAddress()
    {
        WebElement addressList = driver.findElement(deliveryAddress);
        String[] address=addressList.getText().split("[^a-zA-Z0-9']");
        UserInfo userAddress = new UserInfo(address[3],address[5],address[6],address[7],address[11]+" "+address[12],address[9],address[8],address[10],address[13]);
        return userAddress;
    }

    public WebDriver proceedToPayments()
    {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,800)", "");
        Actions builder = new Actions(driver);
        Action mouseOver = builder.moveToElement(driver.findElement(paymentsButton)).click().build();
        mouseOver.perform();
       return driver;
    }
}
