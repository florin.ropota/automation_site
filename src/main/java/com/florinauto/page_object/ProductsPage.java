package com.florinauto.page_object;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import java.util.List;

public class ProductsPage {
    private WebDriver driver;

    private By categories=By.cssSelector("[data-parent=\"#accordian\"]");
    
   // private By productTypes=By.cssSelector("[href*=\"category_products\"]");

    private By productsSelect=By.cssSelector("[class=\"single-products\"]");
    private By addToCart=By.cssSelector("[class*=\"add-to-cart\"]");
    private By productOverlay=By.className("=\"product-overlay\"");
    private By productTypesMen=By.cssSelector("[id=\"Men\"] [href*=\"category_products\"]");
    private By productTypesWomen=By.cssSelector("[id=\"Women\"] [href*=\"category_products\"]");
    private By productTypesKids=By.cssSelector("[id=\"Kids\"] [href*=\"category_products\"]");
    private By productTypes;
    private By continueButton=By.cssSelector("[class*=\"btn-success\"]");
    private By viewCart=By.cssSelector("[href=\"/view_cart\"]");
    public ProductsPage(WebDriver driver) {this.driver = driver;}


 public void closePopUp()
    {
        driver.get("https://automationexercise.com/products");
   }
    public List<WebElement> categoryList()
    {

        return driver.findElements(categories);
    }

    public void selectCategory(String category)
    {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));
        wait.until(ExpectedConditions.elementToBeClickable((categories)));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,400)", "");

        List<WebElement> categoryList = categoryList();
        for (WebElement categoryElement:categoryList)
        {
            if (categoryElement.getText().toUpperCase().equals(category.toUpperCase()))
            {categoryElement.click();}
        }
    }
    public List<WebElement> listOfProductTypes(String category)
    {


            if (category.equals("Men"))
              {productTypes=productTypesMen;}
            else if (category.equals("Women"))
                {productTypes=productTypesWomen;}
                    else {productTypes=productTypesKids;}

        return driver.findElements(productTypes);
    }
    public By productTypesLocator(String category)
    {
        if (category.equals("Men"))
        {productTypes=productTypesMen;}
        else if (category.equals("Women"))
        {productTypes=productTypesWomen;}
        else {productTypes=productTypesKids;}

        return productTypes;
    }
    public void selectProductType(String productType, String category)
    {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));
        wait.until(ExpectedConditions.elementToBeClickable((productTypesLocator(category))));

        List<WebElement> productTypeList = listOfProductTypes(category);
        for (WebElement element:productTypeList)
        {
            if (element.getText().toUpperCase().equals(productType.toUpperCase()))
            {element.click();}
        }
    }
    public List<WebElement> listOfProducts()
    {
        return driver.findElements(productsSelect);
    }

    public void selectProduct(String product)
    {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));
        wait.until(ExpectedConditions.elementToBeClickable((productsSelect)));
        List<WebElement> productsList = listOfProducts();

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,150)", "");
        for (WebElement element:productsList)
        {
            if (element.getText().toUpperCase().contains(product.toUpperCase()))
            {
                Actions builder = new Actions(driver);
                Action mouseOver = builder.moveToElement(element.findElement(addToCart)).build();
                mouseOver.perform();
                WebDriverWait wait1 = new WebDriverWait(driver, Duration.ofSeconds(15));
                wait1.until(ExpectedConditions.elementToBeClickable((addToCart)));
                element.findElement(addToCart).click();

                WebDriverWait wait2 = new WebDriverWait(driver, Duration.ofSeconds(15));
                wait2.until(ExpectedConditions.elementToBeClickable((continueButton)));
                driver.findElement(continueButton).click();
            }
        }
    }
    public WebDriver viewCart()
    {
        List<WebElement> viewCartElements= driver.findElements(viewCart);
     //   Actions builder = new Actions(driver);
      //  Action mouseOver = builder.moveToElement(viewCartElements.get(0)).click().build();
     //   mouseOver.perform();
        viewCartElements.get(0).click();
        return driver;
    }
}
