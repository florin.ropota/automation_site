package com.florinauto.page_object;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CartPage {
    private WebDriver driver;
    private By checkoutButton=By.cssSelector("[class*=\"check_out\"]");

    public CartPage(WebDriver driver) {
        this.driver = driver;
    }

    public void checkProducts()
    {
        //to be completed.
    }
    public WebDriver proceedToCheckout()
    {
        driver.findElement(checkoutButton).click();
        return driver;
    }
}
