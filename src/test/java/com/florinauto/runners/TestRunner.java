package com.florinauto.runners;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
        features = "target/test-classes/resources/features/",
        glue="com/florinauto/stepsdefinitions",
        plugin={"pretty","html:target/Cucumber.html"}
       // tags = "@Register or @PositiveLogin or @NegativeLogin or @Sale"
)

public class TestRunner {

}
