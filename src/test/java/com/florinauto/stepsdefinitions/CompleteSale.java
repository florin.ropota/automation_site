package com.florinauto.stepsdefinitions;

import com.florinauto.model.LoginInfo;
import com.florinauto.model.PaymentInfo;
import com.florinauto.model.UserInfo;
import com.florinauto.page_object.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.ResponseBody;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

import static com.florinauto.stepsdefinitions.Hooks.driver;
import static org.hamcrest.Matchers.equalTo;

public class CompleteSale {

    MainPage main;
    LoginPage login;

    ProductsPage products;

    CartPage cart;
    CheckoutPage checkOut;
    PaymentPage payment;
    @Given("Be on the store main page")
    public void be_on_the_store_main_page() {
        main= new Application(driver).navigateToMainPage();
    }

    @When("Navigate to login page")
    public void navigateToLoginPage() {
        WebDriver driver = main.clickLoginSignup();
        login = new LoginPage(driver);
    }

    @When("Login store {string},{string}")
    public void login_store(String email, String password) {
        LoginInfo loginDetails= new LoginInfo(email,password);
        login.addLoginDetails(loginDetails);
        login.clickLogin();
    }

    @And("Navigate to products")
    public void navigateToProducts() {
        WebDriver driver = main.clickViewProducts();
        products = new ProductsPage(driver);
        products.closePopUp();
    }
    @Then("Select product category {string}")
    public void select_product_category(String category) {
    products.selectCategory(category);
        System.out.println(category);
    }

    @Then("Select product type {string} from {string}")
    public void select_product_type(String productType, String category) {
        products.selectProductType(productType,category);
    }

   @Then("Select two products from the available list {string}, {string}")
   public void select_two_products_from_the_available_list(String product1, String product2) {
       products.selectProduct(product1);
       products.selectProduct(product2);
    }

    @Then("Proceed to cart")
    public void proceed_to_cart() {
        WebDriver driver = products.viewCart();
        cart = new CartPage(driver);
    }

    @And("Verify that the product where added to cart {string}, {string}")
    public void verify_that_the_product_where_added_to_cart(String string, String string2) {
    }


    @Then("Select proceed to checkout")
    public void select_proceed_to_checkout() {
        WebDriver driver = cart.proceedToCheckout();
        checkOut = new CheckoutPage(driver);
    }

    @And("Verify the delivery address")
    public void verifyTheDeliveryAddress() {
        UserInfo address =checkOut.deliveryAddress();
        /*given().contentType("application/json").
                when().get("https://automationexercise.com/api/getUserDetailByEmail?email=testflorin1@test.com").
              then().assertThat().
                .body("title", equalTo(address.title));*/
                /*   .and().body("first_name",equalTo(address.firstName)).
                and().body("last_name",equalTo(address.lastName)).and().body("address1",equalTo(address.address)).
                and().body("country",equalTo(address.country)).and().body("state",equalTo(address.state)).
                and().body("city",equalTo(address.city)).and().body("zipcode",equalTo(address.zipCode));*/

        ResponseBody body = given().contentType("application/json").
                when().get("https://automationexercise.com/api/getUserDetailByEmail?email=testflorin1@test.com").getBody();
        System.out.println(body.asString());


        /*   .and().body("first_name",equalTo(address.firstName)).
                and().body("last_name",equalTo(address.lastName)).and().body("address1",equalTo(address.address)).
                and().body("country",equalTo(address.country)).and().body("state",equalTo(address.state)).
                and().body("city",equalTo(address.city)).and().body("zipcode",equalTo(address.zipCode));*/
    }
    @Then("Select place the order")
    public void select_place_the_order() {
      WebDriver driver = checkOut.proceedToPayments();
      payment = new PaymentPage(driver);
    }

    @Then("Fill the credit card details {string},{string}{string}{string}{string}")
    public void fill_the_credit_card_details(String name, String number, String cvv, String month, String year) {
        PaymentInfo payDetails = new PaymentInfo(name, number, cvv, month, year);
        payment.fillPaymentDetails(payDetails);
    }

    @Then("Verify that the order was placed.")
    public void verify_that_the_order_was_placed() {
        Assert.assertEquals("The payment was not successfully","ORDER PLACED!",payment.orderPlaced());
    }
    @And("Download the invoice")
    public void download_the_invoice() {
        Assert.assertTrue("The invoice was not created", payment.downloadInvoice());
    }
    @And("Verify that the details are correct")
    public void verify_that_the_details_are_correct() {

    }

    @And("Delete the account")
    public void deleteTheAccount() {
        payment.deleteAccount();
    }
}
