package com.florinauto.stepsdefinitions;

import com.florinauto.model.LoginInfo;
import com.florinauto.page_object.Application;
import com.florinauto.page_object.LoginPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import static com.florinauto.stepsdefinitions.Hooks.driver;


public class LoginSteps {

    LoginPage login;
    @Given("Be on login page")
    public void be_on_login_page() {
        login = new Application(driver).navigateToLoginPage();
    }

    @When("Put the email {string} and put the password {string}")
    public void putTheEmailAndPutThePassword(String email, String password) {
        LoginInfo loginDetails= new LoginInfo(email,password);
        login.addLoginDetails(loginDetails);
    }
    @And("Click Login button")
    public void clickLoginButton() {
        login.clickLogin();
    }

    @Then("The Login is successful")
    public void theLoginIsSuccessful() {
        Assert.assertEquals("Error in the Login of the account","Logout", login.verifySuccessLogin());
    }

    @When("Press Logout")
    public void pressLogout() {
        login.logout();
    }

    @Then("The user is logged out")
    public void theUserIsLoggedOut() {
        Assert.assertEquals("Error in the Logout action","Login", login.verifySuccessLogout());
    }

    @Then("The Login is not successful")
    public void theLoginIsNotSuccessful() {
        Assert.assertTrue("Failed tests for wrong credentials",login.verifyWrongLogin());
    }

}
