package com.florinauto.stepsdefinitions;
import com.florinauto.page_object.Application;
import com.florinauto.page_object.RegisterPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import static com.florinauto.stepsdefinitions.Hooks.driver;

public class RegisterSteps {

    RegisterPage register;
    @Given("Be on login or register page")
    public void be_on_login_or_register_page() {
        register = new Application(driver).navigateToRegisterPage();
    }

    @When("Put the username {string} and put the email {string}")
    public void put_the_username_and_put_the_email(String username, String email) {
        register.fillUsername(username);
        register.fillEmail(email);
    }
    @And("Click Signup")
    public void clickSignup() {
        register.clickSignupButton();
    }

    @Then("Account information page is displayed")
    public void accountInformationPageIsDisplayed() {
        boolean success= register.checkSignupWasSuccessful();
        Assert.assertTrue(success);
    }

    @When("Insert gender {string}")
    public void insert_gender(String gender) {
        register.selectGender(gender);
    }

    @And("Insert Password {string}")
    public void insert_password(String password) {
        register.fillPassword(password);
    }

    @And("Insert Date of birth {string}")
    public void insert_date_of_birth(String dateOfBirth) {
        register.fillBirthDate(dateOfBirth);
    }

    @And("Insert First name <firstName>")
    public void insert_first_name_first_name() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }
    @And("Insert First name {string}")
    public void insertFirstName(String firstName) {
        register.fillFirstName(firstName);
    }

    @And("Insert Last name {string}")
    public void insertLastName(String lastName) {
        register.fillLastName(lastName);
    }

    @And("Insert Address {string}")
    public void insertAddress(String address) {
        register.fillAddress(address);
    }

    @And("Select Country {string}")
    public void selectCountry(String country){
        register.fillCountry(country);
    }

    @And("Insert State {string}")
    public void insertState(String state) {
        register.fillState(state);
    }

    @And("Insert City {string}")
    public void insertCity(String city) {
        register.fillCity(city);
    }

    @And("Insert ZipCode {string}")
    public void insertZipCode(String zipCode) {
        register.fillZipCode(zipCode);
    }

    @And("Insert Mobile Number {string}")
    public void insertMobileNumber(String mobileNumber) {
        register.fillMobileNumber(mobileNumber);
    }

  @And("Click Create Account")
     public void clickCreateAccount() {
    register.clickCreateAccount();
   }

    @Then("Account was created successfully")
    public void accountWasCreatedSuccessfully() {
    Assert.assertEquals("Error in the creation of the account","ACCOUNT CREATED!", register.accountCreated());
    }

}
