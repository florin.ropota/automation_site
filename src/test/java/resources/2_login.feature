Feature: Login

  Scenario Outline: Successful Login
    Given Be on login page
    When Put the email <email> and put the password <password>
    And Click Login button
    Then The Login is successful
    When Press Logout
    Then The user is logged out

  Examples:
    | email                  | password |
    | "testflorin1@test.com" |"test1"   |

  Scenario Outline: Wrong Login
    Given Be on login page
    When Put the email <email> and put the password <password>
    And Click Login button
    Then The Login is not successful

    Examples:
      | email                  | password |
      | "testflorin1@test.com" |"test1"   |