Feature: Registration

  Scenario Outline: Successful registration
  Given Be on login or register page
    When Put the username <username> and put the email <email>
    And Click Signup
    Then Account information page is displayed
    When Insert gender <gender>
    And Insert Password <password>
    And Insert Date of birth <dateOfBirth>
    And Insert First name <firstName>
    And Insert Last name <lastName>
    And Insert Address <address>
    And Select Country <country>
    And Insert State <state>
    And Insert City <city>
    And Insert ZipCode <zipCode>
    And Insert Mobile Number <mobileNumber>
    And Click Create Account
    Then Account was created successfully

    Examples:
      | username      | email                  | gender | password | dateOfBirth  | firstName | lastName | address   | country         | state     | city    | zipCode | mobileNumber |  |
      | "testflorin1" | " " | "Male" | "test1"  | "2001-01-01" | "Florin"  | "Florin" | "Adress1" | "United States" | "Florida" | "Miami" | "1"     | "123"        |  |

