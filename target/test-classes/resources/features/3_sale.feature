Feature: Make a sale
  Scenario Outline: Successful sale
    Given Be on the store main page
    When Navigate to login page
    When Login store <email>,<password>
    And Navigate to products
    Then Select product category <category>
    Then Select product type <productType> from <category>
    Then Select two products from the available list <product1>, <product2>
    Then Proceed to cart
    And Verify that the product where added to cart <product1>, <product2>
    Then Select proceed to checkout
    And Verify the delivery address
    Then Select place the order
    Then Fill the credit card details <name>,<number><cvv><month><year>
    Then Verify that the order was placed.
    And Download the invoice
    And Verify that the details are correct
    And Delete the account

    Examples:
      | email                  | password | category | productType | product1             | product2                     | name     | number             | cvv   | month | year   |  |
      | "testflorin1@test.com" | "test1"  | "Men"    | "Jeans"     | "Soft Stretch Jeans" | "Regular Fit Straight Jeans" | "Florin" | "4111111111111111" | "123" | "01"  | "2027" |  |






    Examples:
